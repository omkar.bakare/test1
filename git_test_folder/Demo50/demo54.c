#include<stdio.h>

#define SWAP(n1, n2, type) {type t = n1; n1 = n2; n2 = t;}

void scan_array(float ptr[], int size);
void print_array(float ptr[], int size);
float sum_array(float ptr[], int size);
int search_array(float ptr[], int size, float key);
int binary_search_array(float ptr[], int left, int right, float key);
void sort_array(float ptr[], int size);

int main(void)
{
	int ret;
	float arr[5] = {5.1, 3.2, 7.8, 1.4, 4.2};
	
	print_array(arr, 5);
	//scan_array(arr, 5);
	//print_array(arr, 5);

	printf("sum of array elemets = %.2f\n", sum_array(arr, 5));

	sort_array(arr, 5);
	print_array(arr, 5);
	//	ret = search_array(arr, 5, 7.9);
	ret = binary_search_array(arr, 0, 4, 3.2);
	if(ret == 999)
		printf("Key is not found\n");
	else
		printf("Key is found at index = %d\n", ret);


	return 0;
}


void scan_array(float ptr[], int size)
{
	int i;
	printf("Enter array elements : \n");
	for(i = 0 ; i < size ; i++)
	{
		printf("arr[%d] = ", i);
		//scanf("%f", &ptr[i]);
		scanf("%f", ptr + i);
	}
}


void print_array(float ptr[], int size)
{
	int i;
	printf("Array is :   ");
	for(i = 0 ; i < size ; i++)
		printf("%.1f   ", ptr[i]);
	printf("\n");

}

float sum_array(float ptr[], int size)
{
	int i;
	float sum = 0;
	for(i = 0 ; i < size ; i++)
		sum += ptr[i];
	
	return sum;
}
int search_array(float ptr[], int size, float key)
{
	int i;
	for(i = 0 ; i < size ; i++)
	{
		if(key == ptr[i])
			return i;
	}
	return 999;
}
int binary_search_array(float ptr[], int left, int right, float key)
{
	int mid;

	while(left <= right)
	{
		mid = (right + left) / 2;
		if(ptr[mid] == key)
			return mid;
		if(key < ptr[mid])
			right = mid - 1;
		if(key > ptr[mid])
			left = mid + 1;
	}
	return 999;
}

void sort_array(float ptr[], int size)
{
	int i, j;
	for(i = 0 ; i < size - 1  ; i++)
	{
		for(j = i + 1 ; j < size ; j++)
		{
			if(ptr[i] > ptr[j])
			{
				SWAP(ptr[i], ptr[j], float);
			}
		}
	}

}






















