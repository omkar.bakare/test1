#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#pragma pack(1)

int selection_sort(void * arr, size_t nmemb, size_t size, int(*compare)(const void *, const void *));
int cmp(const void *a1 , const void *a2);
//void test(void *arr, size_t size);

int main()
{
	int i;
	int arr[9] = {1, 9, 8, 5, 3, 4, 11, 9, 2};
	/*---------------------------------------------------*/

	for(i=0; i< 9; i++)
	{
		printf("arr[%d] = %d\n", i, arr[i]);
	}
	/*-------------------------------------------------------*/

	selection_sort(arr, 9, sizeof(int), cmp);	// sorting

	/*-------------------------------------------------------*/

	printf("\n-------------------------\n");
	printf("After sort Array is:\n");

	for(i=0; i< 9; i++)
	{
		printf("arr[%d] = %d\n", i, arr[i]);
		//printf("arr[%d] = %.2lf\n", i, arr[i]);
	}

	return 0;
}

/*********************************************/
int cmp(const void *a1 , const void *a2)
{
	return *(int *) a1 - *(int *) a2;
}


/*********************************************/
/*int cmp(const void *a1 , const void *a2)
{
	return 100.0f * (*a1) - 100.0f * (*a2);
}*/

/**************************************************/
int selection_sort(void * arr, size_t nmemb, size_t size, int(*compare)(const void *, const void *))
{
	int i,j;
	void *temp;
	/*------------------------------------------------*/

	temp = calloc(1, size);

	/*----------------------------------------------*/

	for(i=0; i< nmemb; i++)
	{
		for(j= i + 1; j< nmemb; j++)
		{
			if(compare( (arr +( i *size)), (arr + (j * size))) > 0 )
			{
					memcpy(temp, (arr + (i * size)) , size); 
					memcpy((arr + (i * size)), (arr + (j * size)) , size) ;
					memcpy((arr + (j * size)), temp , size);
			}
		}
	}
	/*---------------------------------------------------------*/
	free(temp);
	temp = NULL;
}
