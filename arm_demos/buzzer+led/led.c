#include"led.h"



void led_init(void)
{
	// make P2.11 as output
	LPC_GPIO1->FIODIR |= BV(LED);
	// off the buzzer
	led_off();
}

void led_off(void)
{
	// make P1.29 = 0	
	LPC_GPIO1->FIOCLR = BV(LED);
}

void led_on(void)
{
	// make P1.29 = 1
	LPC_GPIO1->FIOSET |= BV(LED);	
}
