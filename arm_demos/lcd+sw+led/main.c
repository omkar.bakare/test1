#include "LPC17xx.h"
#include "lcd.h"
#include "led.h"
//#include "switch.h"

#define SW 12

void lcd_print_num(uint8_t line, uint8_t num)
{
	lcd_write(line, CMD);
	lcd_write(num ,DATA);
}

int main()
{
	int count = 3, status;
	lcd_init();
	led_init();

	LPC_GPIO2->FIODIR &= ~BV(SW); //switch pin as input

	lcd_print(LINE1, "`0    Happy");
	delay_ms(1);
	lcd_print(LINE2, "/\"  Gudi Padwa");

//	do
	while(1)
	{
		if((LPC_GPIO2->FIOPIN & BV(SW)) == 0) // if switch pressed
		{
			delay_ms(100); //debouncing 
			if((LPC_GPIO2->FIOPIN & BV(SW)) == 0) // if switch pressed again
			{
				lcd_write(CLR_LCD, CMD);
				lcd_print(LINE1, "Switch pressed");
				delay_ms(10);
				count = count + 1;
				lcd_write(LINE2, CMD);
				lcd_write(('0'+count%10), DATA);
				led_on();
			}
		}
	}
	
//	}while(1);
	
	return 0;
}
