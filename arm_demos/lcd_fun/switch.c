#include "switch.h"



void sw_init()
{
	LPC_GPIO2->FIODIR &= ~BV(SW);
}


int sw_status()
{
	if((LPC_GPIO2->FIODIR & BV(SW)) == 0)	
		return 0;
	else
		return 1;
}
