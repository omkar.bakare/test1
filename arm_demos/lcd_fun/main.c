#include "LPC17xx.h"
#include "lcd.h"
#include "led.h"
//#include "switch.h"

#define SW 12

void lcd_print_num(uint8_t line, uint8_t num)
{
	lcd_write(line, CMD);
	lcd_write(num ,DATA);
}

int main()
{
	int i;
	int count = 9, status;
	lcd_init();
	led_init();

	LPC_GPIO2->FIODIR &= ~BV(SW); //switch pin as input

	lcd_print(LINE1, "   Show Start in  ");
	delay_ms(1);	

	for(i=1; i<11; i++)
	{
		lcd_write(LINE2, CMD);
		lcd_print(LINE2, "         ");
		lcd_write(('0' + (i % 10)), DATA);
		delay_ms(1000);
	}

	delay_ms(2000);
	
	lcd_write(CLR_LCD, CMD);
	lcd_print(LINE1, " Boche Mai Paay  ");
	delay_ms(1);
	lcd_print(LINE2, "   kuch bhi    ");

	return 0;
}
