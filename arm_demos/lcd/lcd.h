#ifndef __LCD_H
#define __LCD_H

#include"LPC17xx.h"

#define LCD_DATA_PORT	LPC_GPIO2
#define LCD_CNTL_PORT	LPC_GPIO1

#define LCD_EN	22
#define LCD_RW	23
#define LCD_RS	24

#define LCD_SHIFT_RIGHT	0x1C
#define LCD_SHIFT_LEFT 	0x18

#define LCD_D7	7
#define LCD_D6	6
#define LCD_D5	5
#define LCD_D4	4


#define LCD_CLR 	0x01
#define LCD_ENT 	0x06
#define LCD_DISP 	0x0C
#define LCD_FUNC 	0x28


#define LINE1 0x80
#define LINE2 0xC0

void lcd_init(void);
void lcd_write_nibble(uint8_t val);
void lcd_write_data(uint8_t val);
void lcd_write_cmd(uint8_t val);
void lcd_busy_chk(void);
void lcd_print(uint8_t line, char *str);


#endif
