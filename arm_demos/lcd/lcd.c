#include "lcd.h"

/****************************************/
void lcd_init(void)
{
	LCD_DATA_PORT->FIODIR |= BV(LCD_D7) | BV(LCD_D6)| BV(LCD_D5) | BV(LCD_D4);

	LCD_CNTL_PORT->FIODIR |= BV(LCD_RW) | BV(LCD_RS) | BV(LCD_EN);

	//lcd_busy_chk();
	delay_ms(200);
	
	// set 4-bit mode
	lcd_write_cmd(LCD_FUNC);
	// display on
	lcd_write_cmd(LCD_DISP);
	// clear display
	lcd_write_cmd(LCD_CLR);
	// set entry mode
	lcd_write_cmd(LCD_ENT);
}

/********************************************/
void lcd_write_nibble(uint8_t val)
{
	//make RW = 0;
	LCD_CNTL_PORT->FIOCLR = BV(LCD_RW);

	//write data d4-d7_
	LCD_DATA_PORT->FIOCLR = BV(LCD_D4) | BV(LCD_D5)| BV(LCD_D6) | BV(LCD_D7);

	LCD_DATA_PORT->FIOSET |= ((uint32_t)val) << LCD_D4;

	//falling edge on EN
	LCD_CNTL_PORT->FIOSET |= BV(LCD_EN);
	delay_ms(1);
	//__NOP();
	LCD_CNTL_PORT->FIOCLR = BV(LCD_EN);
}

/************************************************/
void lcd_write_data(uint8_t val)
{	
		uint8_t high = val >> 4, low = val & 0x0F;
		//make RS = 1			
		LCD_CNTL_PORT->FIOSET |= BV(LCD_RS);

		//write upper nibble
		lcd_write_nibble(high);

		lcd_write_nibble(low);

		lcd_busy_chk();

		delay_ms(3);
}

/***********************************************/
void lcd_write_cmd(uint8_t val)
{
		uint8_t high = val >> 4, low = val & 0x0F;
		//make RS = 0			
		LCD_CNTL_PORT->FIOCLR = BV(LCD_RS);

		//write upper nibble
		lcd_write_nibble(high);

		lcd_write_nibble(low);

		lcd_busy_chk();

		delay_ms(3);
}

/***************************************/
void lcd_busy_chk()
{
	//make D7 as input
	LCD_DATA_PORT->FIODIR &= ~BV(LCD_D7);

	//make RS = 0
	LCD_CNTL_PORT->FIOCLR = BV(LCD_RS);

	//make RW=1 	
	LCD_CNTL_PORT->FIOSET |= BV(LCD_RW) | BV(LCD_EN);

	//read D7 until it is not 0

	while( (LCD_DATA_PORT->FIOPIN & BV(LCD_D7)) != 0 )
			;

	//make EN = 0
	LCD_CNTL_PORT->FIOCLR = BV(LCD_EN);

	//make D7 as output
	LCD_DATA_PORT->FIODIR |= BV(LCD_D7);
}

/****************************************************/
void lcd_print(uint8_t line, char *str)
{
	int i;

	lcd_write_cmd(line);

	for(i=0; str[i] != '\0'; i++)
	{
		lcd_write_data(str[i]);
	}
}




