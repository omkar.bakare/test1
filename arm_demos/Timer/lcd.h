#include"LPC17xx.h"


#define LCD_DATA_PORT	LPC_GPIO2
#define LCD_CNTL_PORT	LPC_GPIO1

#define RS 	24
#define RW 	23
#define EN 	22

#define D4 	4
#define D5 	5
#define D6 	6
#define D7 	7

#define CLR_LCD 	0X01
#define ENT_MODE 	0X06
#define DISP_ON 	0X0C
#define FUN_SET 	0X28

#define CMD		0
#define DATA	1

#define LINE1	 0X80
#define LINE2 	 0XC0


void lcd_init(void);
void lcd_busy_chk();
void write_nibble(uint8_t data);
void lcd_write(uint8_t data, uint8_t cmd);
void lcd_print(uint8_t line, char* str);
