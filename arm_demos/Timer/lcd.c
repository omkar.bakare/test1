#include"lcd.h"

void lcd_init(void)
{
		LCD_DATA_PORT->FIODIR |= BV(D7) | BV(D6) | BV(D5) | BV(D4);

		LCD_CNTL_PORT->FIODIR |= BV(RS) | BV(RW) | BV(EN);

		delay_ms(200);

		lcd_write(FUN_SET,CMD);

		lcd_write(CLR_LCD,CMD);

		lcd_write(DISP_ON,CMD);

		lcd_write(ENT_MODE,CMD);

}

/********************************************/
void lcd_busy_chk()
{
		LCD_DATA_PORT->FIODIR &= ~BV(D7);

		LCD_CNTL_PORT->FIOCLR = BV(RS);

		LCD_CNTL_PORT->FIOSET |= BV(RS) | BV(RW);

		while((LCD_DATA_PORT->FIODIR & BV(D7)) != 0);

		LCD_CNTL_PORT->FIOCLR = BV(EN);

		LCD_DATA_PORT->FIODIR |= BV(D7);
}

/*******************************************/
void write_nibble(uint8_t data)
{
	//RW = 0;
	LCD_CNTL_PORT->FIOCLR = BV(RW);
	//clear data pins
	LCD_DATA_PORT->FIOCLR = BV(D7) | BV(D6) | BV(D5) | BV(D4) ;
	//write value on data pins
	LCD_DATA_PORT->FIOSET |= ( (uint32_t) data) << (D4);
	// EN = 1
	LCD_CNTL_PORT->FIOSET |= BV(EN);
	// delay
	delay_ms(1);	
	// EN =0
	LCD_CNTL_PORT->FIOCLR = BV(EN);

}

/*********************************************/
void lcd_write(uint8_t data, uint8_t cmd)
{
	uint8_t high = (data >> 4), low =(data & 0x0F);

	if(cmd == DATA)
		LCD_CNTL_PORT->FIOSET |= BV(RS);
	else
		LCD_CNTL_PORT->FIOCLR = BV(RS);

	write_nibble(high);

	write_nibble(low);

	lcd_busy_chk();
	
	delay_ms(3);
}

/********************************************/
void lcd_print(uint8_t line, char* str)
{
	int i;

	lcd_write(line,CMD);

	for(i=0; str[i] != '\0'; i++)
			lcd_write(str[i], DATA);
}

